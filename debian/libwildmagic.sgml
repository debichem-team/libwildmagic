<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@


	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!ENTITY dhfirstname "<firstname>Filippo</firstname>">
  <!ENTITY dhsurname   "<surname>Rusconi</surname>">
  <!ENTITY dhdate      "<date>January 23, 2014</date>">
  <!ENTITY dhsection   "<manvolnum>3</manvolnum>">
  <!ENTITY dhemail     "<email>lopippo@debian.org</email>">
  <!ENTITY dhusername  "Filippo Rusconi">
  <!ENTITY dhucpackage "<refentrytitle>LIBWILDMAGIC</refentrytitle>">
  <!ENTITY dhpackage   "libwildmagic">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
<!ENTITY LIBVERSION "5">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2003</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>The &dhpackage package ships a number of libraries
      useful for performing computations in the following fields:
      computer graphics, mathematics, physics, numerical methods, image
      analysis.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <application>&dhpackage;</application> source package.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.</para>

    <para>The <application>&dhpackage;</application> source package is used to create the following packages:

      <itemizedlist>
        <listitem>
         <para><application>&dhpackage;&LIBVERSION;</application> runtime binary package</para>
        </listitem>
        <listitem>
         <para><application>&dhpackage;&LIBVERSION;-dbg;</application> runtime binary package (debugging symbols)</para>
        </listitem>
        <listitem>
         <para><application>&dhpackage;-dev</application> development binary package</para>
        </listitem>
        <listitem>
         <para><application>&dhpackage;-common</application> common data binary package</para>
        </listitem>
        <listitem>
         <para><application>&dhpackage;-examples</application> example binary package</para>
        </listitem>
      </itemizedlist>
    </para>

    <para>
      The <application>&dhpackage;&LIBVERSION;</application> runtime
      binary package ships the following libraries
      
      <itemizedlist>
        <listitem>
         <para> libWm5Core.so </para>
        </listitem>

        <listitem>
          <para> libWm5Applications.so </para>
        </listitem>

        <listitem>
          <para> libWm5Graphics.so </para>
        </listitem>

        <listitem>
          <para> libWm5Imagics.so </para>
        </listitem>

        <listitem>
          <para> libWm5Mathematics.so </para>
        </listitem>

        <listitem>
          <para> libWm5Physics.so </para>
        </listitem>
      </itemizedlist>

    </para>

    <para>
      
      The <application>&dhpackage;-dev</application> development
      binary package ships the following files:
      
      <itemizedlist>
        <listitem>
          <para> Include files, shipped in /usr/include/&dhpackage; </para>
        </listitem>
        </itemizedlist>

    </para>

    <para>
      
      The <application>&dhpackage;-common</application> common data
      files binary package ships the following files:
      
      <itemizedlist>
        <listitem>
          <para> Files contained in the Data directory, shipped in
          /usr/share/&dhpackage/; 
          </para>
        </listitem>
        </itemizedlist>

    </para>

    <para>
      
      The <application>&dhpackage;-examples</application> binary
      package ships a large set of example programs that are shipped
      in /usr/lib/libwildmagic.

    </para>

  </refsect1>
  
  <refsect1>
    <title>AUTHOR</title>

    <para>
      This manual page was written by &dhusername; <&dhemail;> for
      the &debian; system (and may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 3 or any
      later version published by the Free Software Foundation.
    </para>
    <para>
      On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL-3.
    </para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
