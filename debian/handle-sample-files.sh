#!/bin/sh

set -e

# We want to transfer all the sample files into a dedicated directory
# so that the .install file from debian/ knows where to find all
# sample binary files.

for filePath in $(find Sample* -executable -type f)
do
    baseName=$(basename ${filePath})
    echo "baseName: ${baseName}"

    newBaseName=$(basename ${baseName}  .DebugDynamic)

    dirName=$(dirname ${filePath})
    echo "dirName = ${dirName}"

    destDir="SDK/examples/${dirName}"

    mkdir -p ${destDir}
    cp -v ${filePath} ${destDir}/${newBaseName}
done
